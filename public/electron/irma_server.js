const child_process = require('child_process');
const BPromise = require('bluebird');
//const { getAllAttributesExec } = require('./execNames');
const { startIrmaServerExec } = require('./execNames');

const exec = BPromise.promisify(child_process.execFile);

module.exports = function getAllAttributes() {
  return exec(startIrmaServerExec)
};