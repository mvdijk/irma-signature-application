const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));
const child_process = require('child_process');
const exec = BPromise.promisify(child_process.execFile);
const { irmaSignatureVerifyExec } = require('./execNames');

// Adds " to all nummeric bigInts, so that they can be parsed in Javascript
function convertIntToStringInJson(input) {
  return input.replace(/: *(-{0,1}\d+)/g, ':"$1"');
}

function getNonceFromSignature(signature) {
  console.log("6 getNonceFromSignature");
  const signatureString = convertIntToStringInJson(signature);
  try {
    const parsedSignature = JSON.parse(signatureString);
    return parsedSignature.nonce;
  } catch (e) {
    return undefined;
  }
}

function verifySignatureWithoutRequestGo(signature) {
  parsedSignature = JSON.parse(signature);
  irmaSignature = JSON.stringify(parsedSignature.signature)
  // TRY: https://gobyexample.com/json

  console.log("Input send to GO: ");
  return exec(irmaSignatureVerifyExec, [irmaSignature]);
}

function verifySignatureGo(signature, request) {
  console.log("8. verifySignatureGo");
  const requestString = JSON.stringify(request);
  // return exec(irmaSignatureVerifyExec, [signature1, requestString]);
  return exec(irmaSignatureVerifyExec, [signature, requestString]);
}

function verifySignatureWithNonce(nonce, signature, requests) {
  console.log("7. verifySignatureWithNonce");
  //const request = requests[`request-${nonce}`];
  const request = undefined;

  // TODO: add .then(signatureResult => ....
  // const request = undefined;
  if (request === undefined) {
    console.log("REQUESTUNDEFINED");
    return verifySignatureWithoutRequestGo(signature)
      .then(result => (console.log("result: " + result)))
      .catch(error => console.log("error" + error));
  }

  return verifySignatureGo(signature, request.request)
    .then(JSON.parse)
    .then(signatureResult => (Object.assign({}, signatureResult, {
      request,
    })));

  //   .then(console.log(JSON.parse))
  // return verifySignatureGo(signature, "")
  // .then(signatureResult => console.log("signatureResult: " + signatureResult))
  // .then(signatureResult => (Object.assign({}, signatureResult, {
  //   request,
  // })));
}

function verifyRemoteSignature(sigFile, requests) {
  console.log("verifyRemoteSignature");
  console.log(sigFile);

  sigFileObj = JSON.parse(sigFile).signature;
  //console.log("sigFileObj: " + sigFileObj);

  sigFileObjJson = JSON.stringify(sigFileObj)
  //console.log("sigFileObjJson" + sigFileObjJson);

  const nonce = getNonceFromSignature(sigFileObjJson);
  //console.log("nonce: " + nonce);

  // sigFileObjJsonValid = JSON.parse("[" + sigFileObjJson + "]");
  // console.log("validJSON: " + sigFileObjJsonValid);

  return verifySignatureWithNonce(nonce, sigFileObjJson, requests)
  .then(signatureResult => ({ signatureResult, signature }))
  .catch(error => ({
      signatureResult: {
        proofStatus: 'VALID',
      },
      signature: sigFile,
      error,
    })
  );

}

function verifySignature(signature, requests) {
  console.log(".5 verifySignature");

  // console.log("0" + file);
  // const signature1 = JSON.parse(file);
  // console.log("1" + signature1);
  // const signature2 = signature1.signature.signature;
  // console.log("2" + signature2);
  // const signature3 = JSON.stringify(signature2, null, 4);
  // console.log("3" + signature3);

  // OR NONCE JUST FROM BASIC SIGNATURE OBJECT????
  const nonce = getNonceFromSignature(signature);
  console.log("nonce: " + nonce);

  if (nonce === undefined) {
    // Safetycheck so that we're sure that signature is valid json
    // (since we're passing it to exec..)
    return new BPromise.Promise((resolve) => {
      resolve({
        signatureResult: {
          proofStatus: 'INVALID_SYNTAX_NONCE',
        },
        signature: '',
      });
    });
  }

  //     .catch(error => console.log(error));
  return verifySignatureWithNonce(nonce, signature3, requests)
    .then(signatureResult => ({ signatureResult, signature }))
    .catch(error => ({
        signatureResult: {
          proofStatus: 'INVALID_SYNTAX_ERROR',
        },
        signature: '',
        error,
      })
    );
}

  // COULD TRY TO ONLY SEND PATH
module.exports.verifyRemoteSignature = function(sigPath, requests) {
  return fs.readFileAsync(sigPath, 'utf8')
    .then(sigFile => verifyRemoteSignature(sigFile, requests));
};
module.exports.verifySignature = verifySignature;
module.exports.verifyStoredSignature = function(path, requests) {
  return fs.readFileAsync(path, 'utf8')
    .then(signature => verifySignature(signature, requests));
};
