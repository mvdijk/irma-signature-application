package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	irma "github.com/privacybydesign/irmago"
	"github.com/privacybydesign/irmago/server"
	"github.com/privacybydesign/irmago/server/irmaserver"
)

func main() {
	configuration := &server.Configuration{
		// Replace with address that IRMA apps can reach
		URL: "http://192.168.0.113:8088",
		//SchemesPath:    "/home/mvd/MasterThesis/RepositoryISA/irma-signature-application/go/irma_configuration",
		// EnableSSE:      true,
		// StaticSessions: nil,
		// DisableTLS:     true,
	}

	err := irmaserver.Initialize(configuration)
	if err != nil {
		// ...
		fmt.Println("err[1]")
	}

	http.Handle("/irma/", irmaserver.HandlerFunc())
	http.HandleFunc("/createrequest", createFullnameRequest)
	http.HandleFunc("/signaturerequest", createSignatureRequest)

	// Start the server
	fmt.Println("Going to listen on :8088")
	err = http.ListenAndServe(":8088", nil)

	if err != nil {
		fmt.Println("Failed to listen on :8088")
	}
}

func createSignatureRequest(w http.ResponseWriter, r *http.Request) {

	request := irma.NewSignatureRequest("Message (hash)")
	request.Disclose = irma.AttributeConDisCon{ /* request attributes to attach to ABS */ }
	request.Labels = map[int]irma.TranslatedString{}

	//w.Header().Add("Access-Control-Allow-Origin", "http://localhost:3000")

	//JSON Version
	// request := `{
	// 	"@context": "https://irma.app/ld/request/signature/v2",
	// 	"message": "Message to be signed by user",
	// 	"disclose": ...,
	// 	"labels": ...
	//   }`

	sessionPointer, token, err := irmaserver.StartSession(request, func(r *server.SessionResult) {
		fmt.Println("Session done, result: " + server.ToJson(r))
	})
	if err != nil {
		fmt.Println("Error[1]:SignatureRequest")
	}

	fmt.Println("Created session with token ", token)
	fmt.Println("Session Pointer: ", sessionPointer)

	// Send session pointer to frontend, which can render it as a QR
	w.Header().Add("Content-Type", "text/json")

	jsonSessionPointer, _ := json.Marshal(sessionPointer)
	w.Write(jsonSessionPointer)
}

func createFullnameRequest(w http.ResponseWriter, r *http.Request) {
	//w.Header().Add("Access-Control-Allow-Origin", "*")

	request := `{
	    "type": "disclosing",
	    "content": [{ "label": "Full name", "attributes": [ "pbdf.nijmegen.personalData.fullname" ]}]
	}`

	sessionPointer, token, err := irmaserver.StartSession(request, func(r *server.SessionResult) {
		fmt.Println("Session done, result: ", server.ToJson(r))
	})
	if err != nil {
		// ...
		fmt.Println("err[2]")
	}

	fmt.Println("Created session with token ", token)

	// Send session pointer to frontend, which can render it as a QR
	w.Header().Add("Content-Type", "text/json")

	jsonSessionPointer, _ := json.Marshal(sessionPointer)
	w.Write(jsonSessionPointer)
}
