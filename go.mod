module privacybydesign/irmago

go 1.15

require (
	github.com/bwesterb/byteswriter v1.0.0
	github.com/bwesterb/go-atum v1.0.0
	github.com/bwesterb/go-pow v1.0.0
	github.com/bwesterb/go-xmssmt v1.0.0
	github.com/cespare/xxhash v1.1.0
	github.com/coreos/bbolt v1.3.2
	github.com/credentials/safeprime v0.0.0-20180508140535-b04afffdb6d9
	github.com/edsrzf/mmap-go v1.0.0
	github.com/go-errors/errors v1.0.1
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/hashicorp/errwrap v1.0.0
	github.com/hashicorp/go-multierror v1.0.0
	github.com/mhe/gabi v0.0.0-20180228140209-bf5016a5f300
	github.com/nightlyone/lockfile v0.0.0-20180618180623-0ad87eef1443
	github.com/pkg/errors v0.8.1
	github.com/privacybydesign/irmago v0.6.1
	github.com/rainycape/dl v0.0.0-20151222075243-1b01514224a1
	github.com/rs/cors v1.7.0 // indirect
	github.com/templexxx/cpufeat v0.0.0-20180724012125-cef66df7f161
	github.com/templexxx/xor v0.0.0-20181023030647-4e92f724b73b
	github.com/timshannon/bolthold v0.0.0-20190812165541-a85bcc049a2e
	golang.org/x/crypto v0.0.0-20200204104054-c9f3fb736b72
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5
)
