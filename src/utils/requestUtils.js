import bigInt from 'big-integer';

function generateNonce() {
  return bigInt.randBetween(bigInt.zero, bigInt('1e80')).toString();
}

function attributeMapToContent(attributeMap) {
  return Object.keys(attributeMap)
    .map((el) => ({
      label: attributeMap[el].name,
      attributes: [el],
    }));
}

export function createSigrequestFromInput(input) {
  console.log("INPUT2: " + JSON.stringify(input));
  
  // TODO add hash parameter
  return {
    nonce: generateNonce(),
    context: '0',
    document: input.document,
    message: input.message, // message = hash
    content: attributeMapToContent(input.attributes),
    signature: input.signature,
    from: input.from, // TODO remove this input
  };
}

export function generateDate() {
    return Date.now();
}

export function formatTimestamp(ts) {
    const date = new Date(ts);
    const minutes = date.getMinutes() > 9 ? `${date.getMinutes()}` : `0${date.getMinutes()}`;
    const hours = date.getHours() > 9 ? `${date.getHours()}` : `0${date.getHours()}`;
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()} ${hours}:${minutes}`;
}
