const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;
const shell = electron.shell;

export function searchAttributesElectron() {
  ipcRenderer.send('searchAttributes-req');
}

export async function saveSignatureRequestElectron(sigRequest, path) {

  let result = await path;
  // path.then(function(result) {
  //   console.log(result.filePath);
  // });
  console.log(sigRequest);
  console.log(result.filePath);
  //const sigReq = JSON.parse(JSON.stringify(sigRequest));
  const sigReq = JSON.parse(JSON.stringify(sigRequest));
  console.log(sigReq);
  ipcRenderer.send('saveSignatureRequest-req', sigRequest, result.filePath);
}

export function verifySignatureElectron(signature, requests) {
  ipcRenderer.send('verifySignature-req', signature, requests);
}

export function verifyStoredSignatureElectron(path, requests) {
  console.log("3. verifyStoredSignatureElectron");
  console.log("Path: " + path);
  console.log("Requests: " + JSON.stringify(requests));
  ipcRenderer.send('verifyStoredSignature-req', path, requests);
}

export function dragSignatureRequestElectron(request, filename) {
  ipcRenderer.send('dragSignatureRequest-req', request, filename);
}

export function getSignatureSavePath(path) {
  return electron.remote.dialog.showSaveDialog({
    defaultPath: path || undefined,
    title: 'Save IRMA Signature',
    filters: [
      { name: 'IRMA Signature Files', extensions: ['irmasignature'] },
      { name: 'All Files', extensions: ['*'] },
    ],
  });
}

export function getDefaultSavePath() {
  return electron.remote.dialog.showOpenDialog({
    title: 'Select default storage directory',
    properties: ['openDirectory', 'createDirectory', 'noResolveAliases'],
  });
}

export function openExtern(url) {
  return shell.openExternal(url);
}

export function getCommandlineArgument() {
  if (window.location.hostname !== 'localhost' && electron.remote.process.argv.length > 1)
    return electron.remote.process.argv[1];
  return null;
}
