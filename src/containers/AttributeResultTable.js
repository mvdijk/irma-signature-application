import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import flatten from 'flatten';

// Material UI
import {
Table,
TableBody,
TableCell,
TableHead,
TableRow,
} from '@material-ui/core';

class AttributeResultTable extends Component {

  getAttributeDetails(attributes) {
    const { attributeResult } = this.props;
    console.log("disclosed: " + this.props.signature);
    const signature = this.props.signature;
    const disclosedAttributes = attributes;
    const matchingAttrributes = [];
    console.log("LIST1: " + attributes);
    console.log("LIST2: " + signature.disclosed);

    var attributeResultLength = attributeResult.length;
    var disclosedAttributesLength = disclosedAttributes.length;
    console.log("LENGTH1: " + disclosedAttributesLength);

    var id = '';
    var credentialName = '';
    var attributeName = '';
    var value = '';
    var issuerName = '';

    for (var y = 0; y < disclosedAttributesLength; y++) {
      console.log(y);
      console.log("LIST" + disclosedAttributes[y].attributes);  

      for (var i = 0; i < attributeResultLength; i++) {
        //console.log(attributeResult[i].id);
        if(disclosedAttributes[y].attributes == attributeResult[i].id) {
          id = i;
          credentialName = attributeResult[i].credentialName;
          attributeName = attributeResult[i].name;
          value = signature.disclosed[0][y].value.en;
          issuerName = attributeResult[i].issuer;

          matchingAttrributes.push({id, credentialName, attributeName, value, issuerName});
        }
        
      }
    }
    console.log("TABLE" + JSON.stringify(matchingAttrributes));
    return matchingAttrributes;
  }

  getAttributeName(id) {
    // Binary search for the matching element in the attributeResults array
    const { attributeResult } = this.props;
    //console.log("getAttributeName attributeResult: " + JSON.stringify(attributeResult));
    const N = attributeResult.length;
    let low = 0;
    let high = N;
    while (high - low > 1) {
      const mid = Math.floor((high + low) / 2);
      if (attributeResult[mid].id > id)
        high = mid;
      else
        low = mid;
    }

    // And display result
    if (attributeResult[low].id !== id)
      return `Unknown attribute ${id}`;
    return `${attributeResult[low].name} - ${attributeResult[low].credentialName}`;
  }

  genUnmatchedTableData = () => {
    //const credentialList = this.props.attributes;
    const credentialList = this.getAttributeDetails(this.props.attributes);
    console.log("props: " + JSON.stringify(this.props));
    console.log("genUnmatchedTableData credentialList: " + JSON.stringify(credentialList))

    if (credentialList === null)
      return [];

    return credentialList.map(credential => ({
      key: credential.id,
      credentialName: credential.credentialName,
      attributeName: credential.attributeName,
      value: credential.value,
      issuerName: credential.issuerName,
    }));

    // return flatten(
    //   credentialList.map(credential => (
    //     Object.keys(credential)
    //       .map(attributeId => ({
    //         key: attributeId,
    //         credentialName: credential.credentialName,
    //         attributeName: credential.attributeName,
    //         issuerName: credential.issuerName,
    //       }))
    //   ))
    // );

    // TODO: make this less complicated
    // return flatten(
    //   credentialList.map(credential => (
    //     Object.keys(credential.attributes)
    //       .map(attributeId => ({
    //         key: attributeId,
    //         name: this.getAttributeName(attributeId), // TODO convert to name
    //         value: credential.attributes[attributeId],
    //       }))
    //   ))
    // );
  }

  genMatchedTableData = () => {
    const { attributes } = this.props;
    console.log("genMatchedTableData attributes: " + JSON.stringify(attributes));
    return attributes.map(attribute => ({
      key: attribute.disclosedId,
      name: this.getAttributeName(attribute.disclosedId), // TODO use attribute name or label?
      value: attribute.disclosedValue,
    }));
  }

  genTableBody = () => {
    const { matched } = this.props;
    console.log("genTableBody matched " + matched);
    console.log("genTableBody attributs " + JSON.stringify(this.props.attributes));

    if (this.props.attributes === undefined)
      return <TableBody><TableRow /></TableBody>;

    //const tableData = (matched ? this.genMatchedTableData() : this.genUnmatchedTableData());
    const tableData = this.genUnmatchedTableData();
    console.log("genTableBody tableData " + JSON.stringify(tableData));

    return ( // TODO convert to n // TODO convert to nameame
      <TableBody>
        {tableData
          .map(el => (
            <TableRow key={el.key} >
              <TableCell>{el.credentialName}</TableCell>
              <TableCell>{el.attributeName}</TableCell>
              <TableCell>{el.value}</TableCell>
              <TableCell>{el.issuerName}</TableCell>
            </TableRow>
          ))
        }
      </TableBody>
    );
  }

  render() {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell style={{fontWeight: 'bold'}}>Credential</TableCell>
            <TableCell style={{fontWeight: 'bold'}}>Attribute</TableCell>
            <TableCell style={{fontWeight: 'bold'}}>Value</TableCell>
            <TableCell style={{fontWeight: 'bold'}}>Issuer</TableCell>
          </TableRow>
        </TableHead>
        {this.genTableBody()}
      </Table>
    );
  }
}

AttributeResultTable.propTypes = {
  matched: PropTypes.bool.isRequired,
  attributes: PropTypes.arrayOf(PropTypes.object), // Only if there are attributes disclosed
  attributeResult: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  const { attributeSearch } = state;

  return {
    attributeResult: attributeSearch.attributeResult,
  };
}


export default connect(mapStateToProps)(AttributeResultTable);
