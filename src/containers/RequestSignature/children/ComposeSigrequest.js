import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';

// Icons
import Delete from '@material-ui/icons/Delete';
import AddCircle from '@material-ui/icons/AddCircle'
import Save from '@material-ui/icons/Save';
import Description from '@material-ui/icons/Description';

import AttributeDropdown from './AttributeDropdown';
import LocalInfoBox from '../../LocalInfoBox';

// PDF Viewer
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';
import samplePDF from '../../../test.pdf';

// Hashing
import crypto from 'crypto';

// Frontend
import IrmaFrontend from '@privacybydesign/irma-frontend';
const util = require('util');

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class ComposeSigrequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validationForced: false,
      setOpen: false,
      metadata: []
    };
  }

  handleOpen = () => {
    if (this.validateBeforeSigReq(this.props.value)) {
      this.setState({validationForced: true});
      return;
    }

    this.setState({setOpen: true});
  }

  handleClose = () => {
    this.setState({setOpen: false});
  }

  sigRequestPopUp = () => {
    var attr = [];

    const hash = this.hashStringB64(this.props.value.document);

    for (var prop in this.props.value.attributes) {
      console.log('attributes: ' + prop);
      attr.push(prop);
    }
    console.log('array: ' + attr);
    const bodyContent = this.createRequest(hash,attr);

    // TODO: add hash to popup
    // See: https://privacybydesign.github.io/irma-frontend-packages/styleguide/section-examples.html

    const popup = IrmaFrontend.newWeb({
      debugging: true, // Enable to get helpful output in the browser console
      element:   '#irma-web-form',
      showHelper: false,
      language:  'en',
      translations: {
        header:  'Hash: ' + hash,
        loading: 'Just one second please!'
      },
    
      // Back-end options
      session: {
        // Points to IRMA server:
        url: 'http://localhost:8088',
    
        // Define signature request:
        start: {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: bodyContent,
        },

        // Map received 'pointer' and 'token' (from start) to variables (which can be used in result).
        mapping: {
          sessionPtr:      r => r.sessionPtr,
          sessionToken:    r => r.token
        },

        // Read the end result of the session (incl. Attribute-Based Signature).
        result: {
          url: (o, {sessionPtr, sessionToken}) => `${o.url}/session/${sessionToken}/result`,
          parseResponse: r => r.json()
        },
      },
    });

    return popup.start()
    .then(result => this.onChangeSignature(result))
    .catch(error => console.error("Couldn't do what you asked 😢", error));
  }

  // Input: string (Base64 encoding of file) | Output: hash in 'hex' format. TODO UPDATE TO SHA3!!!!
  hashStringB64(base64) {
    //console.log(crypto.getHashes());
    const hash = crypto.createHash('sha256', base64).digest('hex');
    return hash;
  }

  createRequest(message, attributes) {
    const requestOutputJSON = JSON.stringify({
      '@context': 'https://irma.app/ld/request/signature/v2',
      'message': message,
      'disclose': [
        [
          attributes
        ]
      ]
    });

    return requestOutputJSON;
  }


  createData(name, info) {
    return { name, info };
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

  toBase64 = event => {
    const reader = new FileReader();
    if (event.target.files[0] !== undefined) {
      reader.readAsDataURL(event.target.files[0]);
      const date = new Date(event.target.files[0].lastModified);
      const size = this.formatBytes(event.target.files[0].size);
  
      reader.onload = () => {
  
        var metadataRows = [
          this.createData('Name: ', event.target.files[0].name),
          this.createData('Type: ', event.target.files[0].type),
          this.createData('Size: ', size),
          this.createData('Last Modified: ', date.toString()),
          this.createData('Hash: ', this.hashStringB64(reader.result)),
        ];
        this.setState({metadata: metadataRows});
  
        this.onChangeDocument(reader.result);
      };
      reader.onerror = error => {
        console.log(error);
      };
    }
  };

  validate(value) {
    const { name, document, attributes, signature } = value;

    return {
      errorName: !name,
      errorDocument: !document,
      errorAttributes: !attributes || attributes.length === 0,
      errorSignature: !signature,
    };
  }

  validateBeforeSave(value) {
    const { errorName, errorDocument, errorAttributes, errorSignature } = this.validate(value);
    return errorName || errorDocument || errorAttributes || errorSignature;
  }

  validateBeforeSigReq(value) {
    const { errorName, errorDocument, errorAttributes } = this.validate(value);
    return errorName || errorDocument || errorAttributes;
  }

  onChangeName = event => {
    const name = event.target.value;
    this.props.onChange({
      ...this.props.value,
      name,
    });
  }

  onChangeSignature = (result) => {
    console.log("sigResult: " + JSON.stringify(result));
    this.props.onChange({
      ...this.props.value,
      signature: result,
    });
  }

  onChangeDocument = (b64) => {
    console.log(b64);
    this.props.onChange({
      ...this.props.value,
      document: b64,
    });
    console.log(this.props.value);
  }

  onRequestSig = event => {
    this.props.onChange({
      ...this.props.value,
      signature: (this.requestSig()),
    });
  }

  addAttribute = (id, value) => {
    this.props.onChange({
      ...this.props.value,
      attributes: {
        ...this.props.value.attributes,
        [id]: value,
      },
    });
  }

  removeAttribute = (id) => {
    this.props.onChange({
      ...this.props.value,
      attributes: (
        Object.keys(this.props.value.attributes)
          .reduce((acc, val) => {
            if (val !== id)
              acc[val] = this.props.value.attributes[val];
            return acc;
          }, {})),
    });
  }

  onSubmit = () => {
    if (this.validateBeforeSave(this.props.value)) {
      this.setState({validationForced: true});
      return;
    }

    this.props.onSubmit();
  }

  onDrag = (event) => {
    if (this.validateBeforeSave(this.props.value)) {
      this.setState({validationForced: true});
      return;
    }

    this.props.onDrag(event);
  }

  render() {
    const { validationForced, setOpen, metadata } = this.state;
    const { value } = this.props;
    const { errorName, errorDocument, errorAttributes, errorSignature } = this.validate(value);
    // const { errorName, errorMessage, errorAttributes } = this.validateFull(value);

    return (
      <CardContent>
        <div>
          <Dialog
            open={setOpen}
            onEntered={this.sigRequestPopUp}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Signature Request - Scan QR with your IRMA Mobile App"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <body class="irma-web-center-child column">
                  <section class="irma-web-form" id="irma-web-form"></section>
                </body>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <LocalInfoBox text="In this field you write a self-chosen name for the signature request that you are producing. It will be used as name of the file in which this signature request is stored. It will also be used in the history overview of all earlier signature requests. This name is not be part of what will be signed." vertAdjust="27px">
          <TextField
            value={value.name || ''}
            onChange={this.onChangeName}

            label={errorName && validationForced ? 'This field is required' : 'Name:'}
            placeholder={'Custom document name'}
            error={errorName && validationForced}
            required
            fullWidth
            autoFocus
            margin="normal"
          />
        </LocalInfoBox>
        <LocalInfoBox text="Submit a document" vertAdjust="20px">
          <TextField
            onChange={this.toBase64}

            name="file-upload"
            type="file"
            margin="normal"
            error={errorDocument && validationForced}
          />
          {/* <Document 
            file={value.document}
          >
            <Page pageNumber={1} />
          </Document> */}
        </LocalInfoBox>
        <LocalInfoBox text="Metadata" vertAdjust="20px">
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableBody>
                {metadata.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row" style={{fontWeight: 'bold'}}>
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.info}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </LocalInfoBox>
        {(errorAttributes && validationForced ? <Typography style={{ paddingTop: '20px', fontSize: '14px', color: 'red', paddingBottom: '6px' }}>You should select at least one attribute!</Typography> : '')}
        <LocalInfoBox
          text="Here you select one or more attributes of the intended signer, such as name, (e-mail) address, or profession, etc.
        These attributes need to be valid for the intended signer at the moment the signature is created.
        Ultimately, when this signature has been created and is then verified, the validity of these attributes is checked and shown.
        " vertAdjust="20px">
          <AttributeDropdown
            selectedAttributes={value.attributes || {}}
            addAttribute={this.addAttribute}
            removeAttribute={this.removeAttribute}
          />
        </LocalInfoBox>
        {(errorSignature && validationForced ? <Typography style={{ paddingTop: '20px', fontSize: '14px', color: 'red', paddingBottom: '6px' }}>No signature requested!</Typography> : '')}
        <Button size="small" style={{ marginRight: '20px' }} draggable={true} disabled={this.validateBeforeSave(this.props.value)} onDragStart={this.onDrag}>
          <Description style={{ fontSize: '20', marginLeft: '2', marginRight: '10' }} />
          {this.props.value.name}.irmasignature
        </Button>
        <Button size="small" variant="raised" color="primary" onClick={this.handleOpen}>
          Request IRMA Signature
          <AddCircle style={{ fontSize: '20', marginLeft: '10', marginRight: '2' }} />
        </Button>
        <Button size="small" variant="raised" color="primary" onClick={this.onSubmit}>
          Export Document
          <Save style={{ fontSize: '20', marginLeft: '10', marginRight: '2' }} />
        </Button>
        {/* <Button size="small" style={{ marginLeft: '2px', marginRight: '20px' }} variant="raised" onClick={this.props.onDiscard} >
          Clear
          <Delete style={{ fontSize: '20', marginLeft: '10', marginRight: '2' }} />
        </Button> */}
      </CardContent>
    );
  }
}

ComposeSigrequest.propTypes = {
  value: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onDiscard: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onDrag: PropTypes.func.isRequired,
};

export default ComposeSigrequest;
